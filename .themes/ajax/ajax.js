addEvent(window, 'load', importLayout, false);
addEvent(window, 'load', IE6navhover, false);

var xmlDoc;

function IE6navhover()
{
	if ((window.attachEvent) && (document.getElementById("breadcrumbtrail") != null))
	{
		var sfEls = document.getElementById("breadcrumbtrail").getElementsByTagName("LI");
		for (var i=0; i<sfEls.length; i++)
		{
			sfEls[i].onmouseover = function()
			{
				this.className += " over";
			}
			sfEls[i].onmouseout = function()
			{
				this.className = this.className.replace(new RegExp(" over\\b"), "");
			}
		}
	}
}

function importLayout()
{
	if (window.XMLHttpRequest)
	{
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4)
			{
				xmlDoc = xmlHttp.responseXML;
			}
		}
		xmlHttp.open("GET", "gallery.xml", true);
		xmlHttp.send(null)
	}
	else if (document.implementation && document.implementation.createDocument)
	{
		xmlDoc = document.implementation.createDocument("", "layout", null);
		xmlDoc.load("gallery.xml");
	}
	else if (window.ActiveXObject)
	{
		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.validateOnParse = false;
		xmlDoc.resolveExternals = false;
		xmlDoc.load("gallery.xml");
 	}
	else
	{
		alert('Your browser can\'t handle this script');
		return;
	}
}

function reloadPage()
{
	openFolder(xmlDoc.documentElement.getAttribute("id"));
}

function getLayoutNode(elementID)
{
	// check for the documentElement first
	if (xmlDoc.documentElement.getAttribute("id") == elementID)
	{
		return xmlDoc.documentElement;
	}
	// this is the best way, but nothing supports this (yet)
	else if ((xmlDoc.getElementById) && (xmlDoc.getElementById(elementID) != null))
	{
		return xmlDoc.getElementById(elementID);
	}

	// this is the next best way, but only Firefox and Opera support this at the moment
	else if (document.createTreeWalker)
	{
		var nodes = document.createTreeWalker(xmlDoc.documentElement, NodeFilter.SHOW_ELEMENT, null, false);
		var node;

		while ((node = nodes.nextNode()) != null)
		{
			if (node.getAttribute("id") == elementID)
			{
				return node;
			}
			else
			{
				continue;
			}
		}
	}

	// this is possibly the worse way, and it's still not supported by Safari
	else
	{
		// this causes a problem with Safari
		// xmlDocFolders.length == 0
		var xmlDocFolders = xmlDoc.getElementsByTagName("folder");
		var i;

		for (i = 0; i < xmlDocFolders.length; i++)
		{
			if (xmlDocFolders[i].getAttribute("id") == elementID)
			{
				return xmlDocFolders[i];
			}
		}

		var xmlDocImages = xmlDoc.getElementsByTagName("image");

		for (i = 0; i < xmlDocImages.length; i++)
		{
			if (xmlDocImages[i].getAttribute("id") == elementID)
			{
				return xmlDocImages[i];
			}
		}
	}
	return null;
}

function addEvent(elm, evType, fn, useCapture)
{
	if (elm.addEventListener) {
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}
	else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	}
	else {
		elm['on' + evType] = fn;
	}
}

function getElementsByClassName(className, tag, elm)
{
	var testClass = new RegExp("(^|\\s)" + className + "(\\s|$)");
	tag = tag || "*";
	elm = elm || document;
	var elements = (tag == "*" && elm.all)? elm.all : elm.getElementsByTagName(tag);
	var returnElements = [];
	var current;
	var length = elements.length;
	for(var i=0; i<length; i++){
		current = elements[i];
		if(testClass.test(current.className)){
			returnElements.push(current);
		}
	}
	if (returnElements.length > 0)
		return returnElements;
	else
		return null;
}

function getChildNodesByTagName(element, tagName)
{
	var result = Array();

	if(!element.hasChildNodes()) {
		return result;
	}

	for(var i = 0; i < element.childNodes.length; i++)
	{
		// this hack is for Konqueror (and possibly Safari)
		// for some reason the <image> tags are replaced
		// with <img> tags
		if ((element.childNodes[i].nodeName == tagName) ||
			(tagName == "image" && element.childNodes[i].nodeName == "img"))
		{
			result.push(element.childNodes[i]);
		}
	}
	return result;
}

function getNodePath(element, nodePath)
{
	nodePath = (nodePath == undefined) ? '' : nodePath;

	if (element == xmlDoc.documentElement)
	{
		return '.' + nodePath;
	}
	else
	{
		nodePath = element.parentNode.getAttribute("name") + '/' + nodePath;
		return getNodePath(element.parentNode, nodePath);
	}
}

function openFolder(elementID)
{
	var layoutNode = getLayoutNode(elementID);
	var nodePath = getNodePath(layoutNode);

	var breadcrumbDiv = createBreadcrumbTrail(layoutNode);

	var newAlbumsDiv = document.createElement("div");
	newAlbumsDiv.setAttribute("id", "albums");

	var backButton = document.createElement("div")
	backButton.setAttribute("id", "backbutton");

	var selectedDiv = document.createElement("div");
	selectedDiv.setAttribute("id", "selectedalbum");

	var selectedAlbumFolders = getChildNodesByTagName(layoutNode, "folder");
	var selectedAlbumImages = getChildNodesByTagName(layoutNode, "image");
	var j;

	for (j = 0; j < selectedAlbumFolders.length; j++)
	{
		var subFolderContainer = createSubFolderContainer(selectedAlbumFolders[j]);
		selectedDiv.appendChild(subFolderContainer);
	}

	for (j = 0; j < selectedAlbumImages.length; j++)
	{
		var thumbnailContainer = createImageThumbnailContainer(selectedAlbumImages[j]);
		selectedDiv.appendChild(thumbnailContainer);
	}

	newAlbumsDiv.appendChild(selectedDiv);

	document.getElementById("albums").parentNode.replaceChild(newAlbumsDiv, document.getElementById("albums"));

	IE6navhover()
	return false;
}

function checkFolderContents(element)
{
	if (element.hasChildNodes() == false)
	{
		return ".themes/ajax/empty.png";
	}
	else
	{
		var nodePath = getNodePath(getLayoutNode(element.getAttribute("id")));
		var imageChildNodes = getChildNodesByTagName(element, "image");

		if (imageChildNodes.length > 0)
		{
			var imagePath = '.thumbs/'
					+ nodePath
					+ element.getAttribute("name")
					+ '/'
					+ imageChildNodes[0].getAttribute("file");
			return imagePath;
		}
		else
		{
			return ".themes/ajax/folder.png";
		}
	}
}

function createImageThumbnailContainer(imageNode, optClass)
{
	var imageNodeID = imageNode.getAttribute("id");
	var nodePath = getNodePath(getLayoutNode(imageNodeID));

	var classes = (optClass == undefined) ? "thumbnail" : "thumbnail " + optClass;

	var albumImageDiv = document.createElement("div");
	albumImageDiv.className = classes;
	albumImageDiv.setAttribute("id", imageNodeID);

	var imageAnchor = document.createElement("a");
	imageAnchor.setAttribute("href", "index.php?id=" + imageNodeID);
	imageAnchor.onclick = function() { showImage(imageNodeID); return false; };

	var imageSpan = document.createElement("span");
	imageSpan.className =  "image";

	var albumImage = document.createElement("img");
	var imageThumbPath = '.thumbs/' + nodePath  + '/' + imageNode.getAttribute("file");
	albumImage.setAttribute("src", imageThumbPath);
	imageAnchor.appendChild(albumImage);

	var imageNameSpan = document.createElement("span");
	imageNameSpan.className = "name";
	imageNameSpan.appendChild(document.createTextNode(imageNode.getAttribute("file")));

	imageSpan.appendChild(imageAnchor)
	albumImageDiv.appendChild(imageSpan);
	albumImageDiv.appendChild(imageNameSpan);

	return albumImageDiv;
}

function createSubFolderContainer(folderNode)
{
	var folderID = folderNode.getAttribute("id");
	var nodePath = getNodePath(getLayoutNode(folderID));

	// create the main folder div
	var subFolder = document.createElement("div");
	subFolder.className = "folder";
	subFolder.setAttribute("id", folderID);

	var folderAnchor = document.createElement("a");
	folderAnchor.setAttribute("href", "index.php?id=" + folderID);

	// this should allow it to work in IE but makes it harder to follow
	// in things like FireBug since you can't see the onclick for the divs
	folderAnchor.onclick = function() { openFolder(folderID); return false; };

	// create a span for the image
	var folderImageSpan = document.createElement("span");
	folderImageSpan.className = "image";

	// create the image
	var folderImage = document.createElement("img");
	var imagePath = checkFolderContents(folderNode);
	folderImage.setAttribute("src", imagePath);
	folderAnchor.appendChild(folderImage);
	folderImageSpan.appendChild(folderAnchor);

	// create a span for the folder icon
	var iconSpan = document.createElement("span");
	iconSpan.className = "icon";

	// create the icon image
	var iconImage = document.createElement("img");
	iconImage.setAttribute("src", ".themes/ajax/foldericon.png");
	iconSpan.appendChild(iconImage);

	// create a span for the text
	var textSpan = document.createElement("span");
	textSpan.className = "name";

	// create the text
	var textContents = folderNode.getAttribute("name");
	var noImages = getChildNodesByTagName(folderNode, "image").length;
	var noFolders = getChildNodesByTagName(folderNode, "folder").length;

	if ((noImages || noFolders) > 0)
	{
		textContents += " (";
		if (noImages > 0) textContents += noImages;
		if (noFolders > 0) textContents += "+" + noFolders;
		textContents += ")";
	}

	var textNode = document.createTextNode(textContents);
	textSpan.appendChild(textNode);

	// appened them all in order
	subFolder.appendChild(folderImageSpan);
	subFolder.appendChild(iconSpan);
	subFolder.appendChild(textSpan);

	return subFolder;
}

function createExpandedImage(imageNode)
{
	var nodePath = getNodePath(getLayoutNode(imageNode.getAttribute("id")));

	var expandedImageDiv = document.createElement("div");
	expandedImageDiv.setAttribute("id", "expandedimage");

	var linkToDiv = document.createElement("div");
	linkToDiv.setAttribute("id", "linktospan");
	linkToDiv.style.display = "none";

	var linkToAnchor = document.createElement("a");
	linkToAnchor.setAttribute("href", "index.php?id=" + imageNode.getAttribute("id"))
	linkToAnchor.appendChild(document.createTextNode("Link to this image"));
	linkToDiv.appendChild(linkToAnchor);

	var loadingDiv = document.createElement("div");
	loadingDiv.setAttribute("id", "loadingspan");
	loadingDiv.style.display = "block";
	loadingDiv.appendChild(document.createTextNode("Loading image"));

	var imageAnchor = document.createElement("a");
	imageAnchor.onclick = function() { closeImage(); return false };
	imageAnchor.href = "#";

	var imageDiv = document.createElement("div");
	imageDiv.setAttribute("id", "expandedimagespan");
	imageDiv.style.display = "none";

	var albumImage = document.createElement("img");
	var imageThumbPath = nodePath  + '/' + imageNode.getAttribute("file");
	albumImage.setAttribute("src", imageThumbPath);
	imageAnchor.appendChild(albumImage);
	imageDiv.appendChild(imageAnchor);

	var imageNameDiv = document.createElement("div");
	imageNameDiv.className = "name";
	imageNameDiv.appendChild(document.createTextNode(imageNode.getAttribute("file")));

	expandedImageDiv.appendChild(linkToDiv);
	expandedImageDiv.appendChild(loadingDiv);
	expandedImageDiv.appendChild(imageDiv);
	expandedImageDiv.appendChild(imageNameDiv);

	var expandedImage = new Image();
	expandedImage.onload = function()
							{
								loadingDiv.style.display = "none";
								imageDiv.style.display = "";
								linkToDiv.style.display = "";
							};
	expandedImage.src = imageThumbPath;

	return expandedImageDiv;
}

function createBreadcrumbTrail(element)
{
	var breadcrumbTrailDiv = document.createElement("div");
	breadcrumbTrailDiv.setAttribute("id", "breadcrumbtrail");

	var nodePath = getNodePath(element) + element.getAttribute("name");

	var paths = nodePath.split('/');
	var currentPath = '';

	for (var i = 0; i < paths.length - 1; i++)
	{
		currentPath += paths[i] + '/';

		var breadcrumbMenu = createBreadcrumbMenu(currentPath, paths[i + 1]);
		breadcrumbTrailDiv.appendChild(breadcrumbMenu);
	}

	if (document.getElementById("breadcrumbtrail"))
	{
		document.getElementById("albums").parentNode.replaceChild(breadcrumbTrailDiv, document.getElementById("breadcrumbtrail"));
	}
	else
	{
		document.getElementById("albums").parentNode.insertBefore(breadcrumbTrailDiv, document.getElementById("albums"));
	}
}

function createBreadcrumbMenu(path, currentFolder)
{
	var breadcrumbEntry = document.createElement("ul");
	breadcrumbEntry.className = "trailentry";

	var menuLi = document.createElement("li");
	menuLi.className = "currentfolder";

	var dropMenu = document.createElement("ul");
	menuLi.appendChild(dropMenu);

	var layoutNode = selectLayoutNodeFromPath(path);

	for (var i = 0; i < layoutNode.childNodes.length; i++)
	{
		var node = layoutNode.childNodes[i];

		// these will all be hidden until the :hover
		if (node.nodeName == "folder")
		{
			var dropMenuItem = createBreadcrumbItem(node, currentFolder);
			dropMenu.appendChild(dropMenuItem);
		}

		// this is for the current folder
		if ((node.nodeName == "folder") && (node.getAttribute("name") == currentFolder))
		{
			var dropMenuAnchor = document.createElement("a");
			dropMenuAnchor.href = "index.php?id=" + dropMenuItem.getAttribute("id");

			var nameSpan = document.createElement("span");
			nameSpan.className = "name";
			nameSpan.appendChild(document.createTextNode(node.getAttribute("name")));

			// create a span for the folder icon
			var iconSpan = document.createElement("span");
			iconSpan.className = "icon";

			// create the icon image
			var iconImage = document.createElement("img");
			iconImage.setAttribute("src", ".themes/ajax/foldericon.png");
			iconSpan.appendChild(iconImage);

			dropMenuAnchor.appendChild(nameSpan);
			dropMenuAnchor.appendChild(nameSpan);

			menuLi.insertBefore(dropMenuAnchor, menuLi.firstChild);

			breadcrumbEntry.appendChild(menuLi);
		}
	}

	return breadcrumbEntry;
}

function createBreadcrumbItem(node, currentFolder)
{
	var dropMenuItem = document.createElement("li");
	dropMenuItem.setAttribute("id", node.getAttribute("id"));

	var dropMenuAnchor = document.createElement("a");
	dropMenuAnchor.href = "index.php?id=" + dropMenuItem.getAttribute("id");
	dropMenuAnchor.onclick = function() { openFolder(dropMenuItem.getAttribute("id")); return false; };
	var iconSpan;

	if (node.hasChildNodes())
	{
		// create a span for the folder icon
		iconSpan = document.createElement("span");
		iconSpan.className = "icon";

		// create the icon image
		var iconImage = document.createElement("img");
		iconImage.setAttribute("src", ".themes/ajax/foldericon.png");
		iconSpan.appendChild(iconImage);

		dropMenuAnchor.appendChild(iconSpan);
	}
	else
	{
		dropMenuItem.setAttribute("class", "empty");

		// create a span for the folder icon
		iconSpan = document.createElement("span");
		iconSpan.className = "icon";

		// create the icon image
		var iconImage = document.createElement("img");
		iconImage.setAttribute("src", ".themes/ajax/emptyicon.png");
		iconSpan.appendChild(iconImage);
		dropMenuItem.appendChild(iconSpan);
	}

	// we do this so we can show the user the currently selected folder in the drop down
	if (node.getAttribute("name") == currentFolder)
	{
		dropMenuItem.className = "menuentry selected";
	}
	else
	{
		dropMenuItem.className = "menuentry";
	}

	var nameSpan = document.createElement("span");
	nameSpan.className = "name";
	nameSpan.appendChild(document.createTextNode(node.getAttribute("name")));
	dropMenuAnchor.appendChild(nameSpan);

	dropMenuItem.appendChild(dropMenuAnchor);
	return dropMenuItem;
}

function selectLayoutNodeFromPath(path, node)
{
	node = (node == undefined) ? xmlDoc.documentElement : node;

	if(path == "./")
	{
		return xmlDoc.documentElement;
	}
	else if (path != '')
	{
		path = (path.indexOf("./") == 0) ? path.substring(2) : path;
		var currentPath = path.split('/')[0];
		var nodes = getChildNodesByTagName(node, "folder");

		for (var i = 0; i < nodes.length; i++)
		{
			if ((nodes[i].getAttribute("name") == currentPath))
			{
				return selectLayoutNodeFromPath(path.replace(currentPath + '/', ''), nodes[i]);
			}
			else
			{
				continue;
			}
		}
	}
	else
	{
		return node;
	}
}

function showImage(imageID)
{
	var expandedImage = createExpandedImage(getLayoutNode(imageID));

	if (document.getElementById("expandedimage"))
	{
		document.getElementById("expandedimage").parentNode.replaceChild(expandedImage, document.getElementById("expandedimage"));
	}
	else
	{
		document.getElementById("selectedalbum").parentNode.insertBefore(expandedImage, document.getElementById("selectedalbum"));
	}

	var lastSelectedImage = getElementsByClassName("selected", "div", document);

	if (lastSelectedImage)
	{
		lastSelectedImage[0].className = "thumbnail";
	}

	document.getElementById(imageID).className += " selected";
}

function closeImage()
{
	document.getElementById("expandedimage").parentNode.removeChild(document.getElementById("expandedimage"));

	var lastSelectedImage = getElementsByClassName("selected", "div", document);

	if (lastSelectedImage)
		lastSelectedImage[0].className = "thumbnail";
}





function getCookie(name) {
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;

	if ((!start) && (name != document.cookie.substring(0, name.length)))
	{
		return null;
	}

	if (start == -1) return null;

	var end = document.cookie.indexOf(';', len);

	if (end == -1) end = document.cookie.length;

	return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
	var today = new Date();
	today.setTime(today.getTime());

	if (expires) {
		expires = expires * 1000 * 60 * 60 * 24;
	}

	var expires_date = new Date(today.getTime() + (expires));

	document.cookie = name + '=' + escape(value) +
		((expires) ? ';expires='+expires_date.toGMTString() : '') + //expires.toGMTString()
		((path) ? ';path=' + path : '') +
		((domain) ? ';domain=' + domain : '') +
		((secure) ? ';secure' : '');
}

function deleteCookie(name, path, domain) {
	if (getCookie(name))
		document.cookie = name + '=' +
			((path) ? ';path=' + path : '') +
			((domain) ? ';domain=' + domain : '') +
			';expires=Thu, 01-Jan-1970 00:00:01 GMT';
}