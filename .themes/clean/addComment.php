<?php
	require_once('../../db.inc');

	$image_md5 = mysql_real_escape_string($_POST['image_id']);
	$ajax = mysql_real_escape_string($_POST['ajax']);
	$name = mysql_real_escape_string($_POST['name']);
	$email = mysql_real_escape_string($_POST['email']);
	$web = mysql_real_escape_string($_POST['web']);
	$comment = mysql_real_escape_string($_POST['comment']);
	$user_ip = mysql_real_escape_string($_SERVER['REMOTE_ADDR']);
	$approved = "1";

	$query = "SELECT image_id FROM images WHERE image_md5 = '$image_md5'";
	$select_result = mysql_query($query);

	if ($select_result)
	{
		$image_id = mysql_fetch_row($select_result);

		$query = "
			INSERT INTO	comments
			(			image_id
			,			user_name
			,			user_www
			,			user_email
			,			user_comment
			,			user_ip
			,			comment_rating
			,			approved
			)
			VALUES
			(			$image_id[0]
			,			'$name'
			,			'$web'
			,			'$email'
			,			'$comment'
			,			'$user_ip'
			,			0
			,			$approved
			)";

		$insert_result = mysql_query($query);
		if ($insert_result)
		{
			header('Content-Type: text/xml');
			echo "<success><message>Comment added successfully</message></success>";
		}
		else
		{
			header('Content-Type: text/xml');
			echo "<error><message>Error inserting comment</message>\n"
				."<message>".mysql_error()."</message>\n"
				."<message>".$query."</message>\n</error>";
		}
	}
	else
	{
		header('Content-Type: text/xml');
		echo "<error><message>Image md5 doesn't exist</message></error>";
	}

?>
