var galleryXML;
var aniSpeed = 1000;

$(document).ready( function() {
	$.get("gallery.xml",
		null,
		function(xml) {
			galleryXML = xml;
			runSetup();
			preloadThumbnailImages();
		}
	);
});

function runSetup()
{
	$("body h1 a").click( function()
		{
			showWelcome();
			return false;
		}
	);

	$("#nav a").click( function()
		{
			openFolder( $(this).parent().attr("id") );
			return false;
		}
	);

	if ( $("#nav li img").length > 0 )
	{
		$("#nav li img").click( function()
			{
				toggleFolder( $(this).parent().attr("id") );
			}
		);
	}
	else
	{
		$("#nav li a").css("margin-left", 0);
	}
}

function openFolder(folderID)
{
	var node = $("#"+folderID, galleryXML);

	if ( $("#welcomehelp").is(":visible") )
	{
		$("#welcomehelp").slideUp(aniSpeed, function() { showFolder(folderID); } );
	}
	else
	{
		showFolder(folderID);
	}

	return false;
}

function showFolder(folderID)
{
	// fade out the old album if one exists
	if ($("#album").length > 0)
	{
		$("#album").slideUp(aniSpeed, function() {
										$("#album").remove();
										showFolder(folderID);
									});
		return;
	}

	// show the sub-folders if there are any and hidden
	if ( ($("#" + folderID + " ul").length > 0) && ( $("#" + folderID + " ul").is(":hidden") ) )
	{
		toggleFolder(folderID);
	}

	// get the node from the XML file
	var galleryXmlNode = $("#"+folderID, galleryXML);
	// get the name of the node (folder)
	var folderName = galleryXmlNode.attr("name");
	// get the number of sub-folders
	var folderChildren = galleryXmlNode.children("folder").length;
	// get the number of images
	var imageChildren = galleryXmlNode.children("image").length;

	var folderId;
	var i;
	var child;

	// append the album div
	$(document.body).createAppend(
		'div', { id: 'album' }, [
			'p', { id: 'albumtitle' }, folderName
		]
	);

	// if we have any folders then append them
	if ( folderChildren > 0 )
	{
		// get a list of the folders
		child = galleryXmlNode.children("folder");

		for (i = 0; i < folderChildren; i++)
		{
			folderId = child.attr("id");
			folderName = child.attr("name");
			// append the folder to the album div
			$("#album").createAppend(
				'div', { className: "folder" }, [
					'p', { }, [
						'a', {
							href: "index.php?id="+folderId,
							onclick: function() { openFolder( folderId ); return false; }
						}, folderName
					]
				]
			);

			// move on to the next one
			child = child.next();
		}
	}

	// if we have any image then append them
	if ( imageChildren > 0 )
	{
		// get a list of the folders
		child = galleryXmlNode.children("image");

		for (i = 0; i < imageChildren; i++)
		{
			var imageFile = child.attr("file");
			var imageId = child.attr("id");
			var imageLoc = getImageFilePath( imageId );
			var imageThumbLoc = "./.thumbs/" + imageLoc;
			folderId = child.parent().attr("id");

			// append the folder to the album div
			$("#album").createAppend(
				'div', { className: "thumbnail" }, [
					'span', { className: "image" }, [
						'a', {
							href: imageLoc,
							className: "thickbox",
							rel: "" + folderId
						}, [
							'img', { src: imageThumbLoc, alt: imageFile }, "",
						]
					],
					'span', { className: "title" }, [
						'a', {
							href: imageLoc,
							onclick: function() { openImage( imageId ); return false; }
						}, imageFile
					]
				]
			);

			// move on to the next one
			child = child.next();
		}
	}

	$("#album .folder a").click( function()
		{
			openFolder( $(this).parent().attr("id") );
			return false;
		}
	);

	tb_init('a.thickbox');

	$("#album").slideDown(aniSpeed);
	return;
}

function openImage(imageId)
{
	alert( getImageFilePath(imageId) );
}

function getImageFilePath(imageId)
{
	var imageNode = $("#"+imageId, galleryXML);
	var parentNodes = imageNode.parents("folder");

	var imageFilePath = "";

	for (var i = 0; i < parentNodes.length; i++)
	{
		imageFilePath =  $(parentNodes[i]).attr("name") + "/" + imageFilePath;
	}

	imageFilePath = imageFilePath + imageNode.attr("file");
	imageFilePath = escape( imageFilePath );

	return imageFilePath;
}

function toggleFolder(folderID)
{
	if ( $("#" + folderID + " ul").is(":visible") )
	{
		$("#" + folderID + " ul").slideUp(aniSpeed);
		$("#" + folderID).removeClass("open");
	}
	else
	{
		$("#" + folderID + " ul").slideDown(aniSpeed);
		$("#" + folderID).addClass("open");
	}
}

function showWelcome()
{
	if ( $("#album").length > 0 )
	{
		$("#album").slideUp(aniSpeed, function() {
										$("#album").remove();
										showWelcome();
									});
	}
	else if ( $("#welcomehelp").is(":hidden") )
	{
		$("#welcomehelp").slideDown(aniSpeed);
	}

	return;
}

function preloadThumbnailImages()
{
	var galleryImages = $("image", galleryXML);
	var galleryImageFiles = Array();

	for (var i = 0; i < galleryImages.length; i++)
	{
		var gall = $(galleryImages[i]);
		var imageFilePath = "./.thumbs/" + getImageFilePath( gall.attr("id") );
		galleryImageFiles.push( imageFilePath );
	}

	$(document.createElement('img')).bind('load',
		function()
		{
			if(galleryImageFiles[0])
			{
				this.src = galleryImageFiles.shift();
			}
		}
	).trigger('load');
}
