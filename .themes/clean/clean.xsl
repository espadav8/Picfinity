<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output
		method="xml"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	/>

	<xsl:param name="title" />
	<xsl:param name="post_id" />
	<xsl:param name="themename">clean</xsl:param>

	<xsl:template match="/">
		<html>
			<head>
				<title><xsl:value-of select="$title" /></title>

				<link rel="stylesheet" type="text/css">
					<xsl:attribute name="href"><xsl:value-of select="concat('.themes/', $themename, '/', $themename, '.css')" /></xsl:attribute>
				</link>
				<script type="text/javascript">
					<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/jquery-1.2.js')" /></xsl:attribute>
				</script>
				<script type="text/javascript">
					<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/jquery.flydom-3.0.8.js')" /></xsl:attribute>
				</script>
				<script type="text/javascript">
					<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/thickbox-3.1.js')" /></xsl:attribute>
				</script>
				<script type="text/javascript">
					<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/', $themename, '.js')" /></xsl:attribute>
				</script>
			</head>

			<body>
				<h1>
					<a href="index.php">
						<xsl:value-of select="$title" />
					</a>
				</h1>

				<xsl:apply-templates select="layout" mode="nav" />

				<xsl:choose>
					<xsl:when test="$post_id = 0">
						<xsl:apply-templates select="layout" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="layout" mode="post"/>
					</xsl:otherwise>
				</xsl:choose>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="layout">
		<div id="welcomehelp">
			<p>
				Welcome to the <xsl:value-of select="$title" /> image gallery. To the left you'll see
				all the main folders in this gallery, to look inside one please just click on the name.
				This will open up the folder in this space as well as expanding the folder structure
				should there be any sub-folders. If you just wish to expand the folder just click on the
				[+] next to the folder name.
			</p>
			<p>
				Enjoy looking around.
			</p>
		</div>
	</xsl:template>

	<xsl:template name="replaceCharsInString">
		<xsl:param name="stringIn"/>
		<xsl:param name="charsIn"/>
		<xsl:param name="charsOut"/>
		<xsl:choose>
			<xsl:when test="contains($stringIn,$charsIn)">
				<xsl:value-of select="concat(substring-before($stringIn,$charsIn),$charsOut)"/>
				<xsl:call-template name="replaceCharsInString">
					<xsl:with-param name="stringIn" select="substring-after($stringIn,$charsIn)"/>
					<xsl:with-param name="charsIn" select="$charsIn"/>
					<xsl:with-param name="charsOut" select="$charsOut"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$stringIn"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="nl2br">
		<xsl:param name="contents" />
		<xsl:choose>
			<xsl:when test="contains($contents, '&#10;')">
					<xsl:value-of select="substring-before($contents, '&#10;')" />
					<br />
					<xsl:call-template name="nl2br">
						<xsl:with-param name="contents" select="substring-after($contents, '&#10;')" />
					</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
					<xsl:value-of select="$contents" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="layout" mode="nav">
		<div id="nav">
			<ul>
				<xsl:apply-templates select="folder" mode="nav" />
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="folder" mode="nav">
			<li>
				<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
				<xsl:if test="count(folder) > 0">
					<img alt="Expand folder">
						<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/plus.png')" /></xsl:attribute>
					</img>
				</xsl:if>
				<a>
					<xsl:attribute name="href">index.php?id=<xsl:value-of select="@id" /></xsl:attribute>
					<xsl:value-of select="@name" />
				</a>
				<xsl:if test="count(folder) > 0">
					<ul>
						<xsl:apply-templates select="folder" mode="nav" />
					</ul>
				</xsl:if>
			</li>
	</xsl:template>

</xsl:stylesheet>
