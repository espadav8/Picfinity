<?php
	require_once('../../db.inc');

	header('Content-Type: text/xml');
	header("Cache-Control: no-cache, must-revalidate");

	$image_md5 = mysql_real_escape_string($_GET['md5']);

	$query = "
			SELECT		user_name
			,			user_www
			,			user_email
			,			user_comment
			,			comment_rating
			,			date_added
			FROM			comments
			INNER JOIN	images
			WHERE		images.image_id = comments.image_id
			AND			images.image_md5 = '$image_md5'
			ORDER BY		date_added DESC";

	$result = mysql_query($query);

	if ($result)
	{
		$comments_xml = "<?xml version=\"1.0\"?>"
					. "<comments>";
		while ($row = mysql_fetch_assoc($result))
		{
			$comments_xml .= "<comment>"
						. "<name>".stripslashes($row['user_name'])."</name>"
						. "<www>".stripslashes($row['user_www'])."</www>"
						. "<email>".stripslashes($row['user_email'])."</email>"
						. "<comment_text>".stripslashes($row['user_comment'])."</comment_text>"
						. "<rating>".stripslashes($row['comment_rating'])."</rating>"
						. "<added>".stripslashes($row['date_added'])."</added>"
						. "</comment>";
		}
		$comments_xml .= "</comments>";

		echo $comments_xml;
	}
	else
	{

		echo "<error><message>Error in getting the results</message></error>";
	}
?>
