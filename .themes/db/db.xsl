<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output
		method="xml"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	/>

	<xsl:param name="title" />
	<xsl:param name="post_id" />
	<xsl:param name="themename">db</xsl:param>

	<xsl:template match="/">
		<html>
			<head>
				<title><xsl:value-of select="$title" /></title>

				<link rel="stylesheet" type="text/css">
					<xsl:attribute name="href"><xsl:value-of select="concat('.themes/', $themename, '/', $themename, '.css')" /></xsl:attribute>
				</link>
				<script type="text/javascript">
					<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/', $themename, '.js')" /></xsl:attribute>
				</script>
			</head>

			<body>
				<h1>
					<a href="index.php" onclick="reloadPage();return false;">
						<xsl:value-of select="$title" />
					</a>
				</h1>

				<xsl:choose>
					<xsl:when test="$post_id = 0">
						<xsl:apply-templates select="layout" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="layout" mode="post"/>
					</xsl:otherwise>
				</xsl:choose>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="layout">
		<xsl:apply-templates select="themes" />

		<div id="breadcrumbtrail"></div>

		<div id="albums">
			<xsl:apply-templates select="folder" />
			<xsl:apply-templates select="image" />
		</div>
	</xsl:template>

	<xsl:template match="themes">
		<div id="themes">
			<form id="themeform" action="index.php" method="post">
				<fieldset>
					<select name="theme" onchange="submit();">
						<option value="">Please choose a theme</option>
						<xsl:apply-templates select="theme" />
					</select>
					<button type="submit">Go</button>
				</fieldset>
			</form>
		</div>
	</xsl:template>

	<xsl:template match="theme">
		<option>
			<xsl:if test="@name = $themename">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:value-of select="@name" />
		</option>
	</xsl:template>

	<xsl:template match="folder">
		<div>
			<xsl:choose>
				<xsl:when test="count(folder) = 0 and count(image) = 0">
					<xsl:attribute name="class">folder empty</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="class">folder</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<span class="image">
				<a>
					<xsl:if test="count(folder) > 0 or count(image) > 0">
						<xsl:attribute name="href">index.php?id=<xsl:value-of select="@id" /></xsl:attribute>
						<xsl:attribute name="onclick">openFolder('<xsl:value-of select="@id" />'); return false;</xsl:attribute>
					</xsl:if>

					<xsl:choose>
						<xsl:when test="count(image) > 0">
							<xsl:variable name="path">
								<xsl:call-template name="replaceCharsInString">
      								<xsl:with-param name="stringIn">
      									<xsl:text>.thumbs/</xsl:text><xsl:apply-templates select="ancestor::folder" mode="getpath" /><xsl:value-of select="concat(@name, '/', image[1]/@file)" />
      								</xsl:with-param>
      								<xsl:with-param name="charsIn" select="' '"/>
      								<xsl:with-param name="charsOut" select="'%20'"/>
      							</xsl:call-template>
							</xsl:variable>
							<img alt="Sample Image">
								<xsl:attribute name="src"><xsl:value-of select="$path" /></xsl:attribute>
								<xsl:attribute name="alt"><xsl:value-of select="image[1]/@file" /></xsl:attribute>
							</img>
						</xsl:when>
						<xsl:when test="count(folder) > 0">
							<img alt="Image Folder">
								<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/folder.png')" /></xsl:attribute>
							</img>
						</xsl:when>
						<xsl:otherwise>
							<img alt="Empty Folder">
								<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/empty.png')" /></xsl:attribute>
							</img>
						</xsl:otherwise>
					</xsl:choose>
				</a>
			</span>
			<xsl:if test="count(folder) > 0 or count(image) > 0">
				<span class="icon">
					<img alt="Folder Icon">
						<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/foldericon.png')" /></xsl:attribute>
					</img>
				</span>
			</xsl:if>
			<span class="name">
				<xsl:value-of select="@name" />
				<xsl:choose>
					<xsl:when test="(count(folder) > 0) and (count(image) > 0)">
						<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>(<xsl:value-of select="count(image)" />+<xsl:value-of select="count(folder)" />)
					</xsl:when>
					<xsl:when test="count(folder) > 0">
						<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>(+<xsl:value-of select="count(folder)" />)
					</xsl:when>
					<xsl:when test="count(image) > 0">
						<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>(<xsl:value-of select="count(image)" />)
					</xsl:when>
				</xsl:choose>
			</span>
		</div>
	</xsl:template>

	<xsl:template match="image">
		<xsl:param name="selectedID">0</xsl:param>
		<xsl:variable name="path">
			<xsl:call-template name="replaceCharsInString">
				<xsl:with-param name="stringIn">
					<xsl:text>.thumbs/</xsl:text><xsl:apply-templates select="ancestor::folder" mode="getpath" /><xsl:value-of select="@file" />
				</xsl:with-param>
				<xsl:with-param name="charsIn" select="' '"/>
				<xsl:with-param name="charsOut" select="'%20'"/>
			</xsl:call-template>
		</xsl:variable>

		<div class="thumbnail">
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:if test="@id = $selectedID">
				<xsl:attribute name="class">thumbnail selected</xsl:attribute>
			</xsl:if>
			<span class="image">
				<a>
					<xsl:attribute name="onclick">showImage('<xsl:value-of select="@id" />'); return false;</xsl:attribute>
					<xsl:attribute name="href">index.php?id=<xsl:value-of select="@id" /></xsl:attribute>
					<img>
						<xsl:attribute name="alt"><xsl:value-of select="@file" /></xsl:attribute>
						<xsl:attribute name="src"><xsl:value-of select="$path" /></xsl:attribute>
					</img>
				</a>
			</span>
			<span class="name"><xsl:value-of select="@file" /></span>
		</div>
	</xsl:template>


	<xsl:template match="layout" mode="post">
		<xsl:variable name="folder">
			<xsl:choose>
				<xsl:when test="count(//folder[@id = $post_id]) = 1">1</xsl:when>
				<xsl:when test="@id = $post_id">2</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>


		<xsl:apply-templates select="themes" />

		<xsl:choose>
			<!-- the passed in ID is an image -->
			<xsl:when test="$folder = 0">
				<xsl:apply-templates select="//image[@id = $post_id]" mode="post" />
			</xsl:when>

			<!-- the passed in ID is an folder -->
			<xsl:when test="$folder = 1">
				<xsl:apply-templates select="//folder[@id = $post_id]" mode="post" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="folder" mode="post">
		<div id="breadcrumbtrail">
			<xsl:apply-templates select="self::*" mode="breadcrumbtrail" />
		</div>
		<div id="albums">
			<div id="selectedalbum">
				<xsl:apply-templates select="folder"/>
				<xsl:apply-templates select="image"/>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="folder" mode="breadcrumbtrail">
		<xsl:if test="parent::folder">
			<xsl:apply-templates select="parent::folder" mode="breadcrumbtrail" />
		</xsl:if>

		<xsl:variable name="currentName"><xsl:value-of select="@name" /></xsl:variable>

		<ul class="trailentry">
			<li class="currentfolder">
				<a>
					<xsl:attribute name="onclick">openFolder('<xsl:value-of select="@id" />'); return false;</xsl:attribute>
					<xsl:attribute name="href">index.php?id=<xsl:value-of select="@id" /></xsl:attribute>
					<span class="icon">
						<img alt="Folder Icon">
							<xsl:attribute name="src"><xsl:value-of select="concat('.themes/', $themename, '/foldericon.png')" /></xsl:attribute>
						</img>
					</span>
					<span class="name"><xsl:value-of select="@name" /></span>
				</a>
				<ul>
					<xsl:for-each select="parent::*/folder">
						<li>
							<xsl:choose>
								<xsl:when test="$currentName = @name">
									<xsl:attribute name="class">menuentry selected</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class">menuentry</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<a>
								<xsl:attribute name="onclick">openFolder('<xsl:value-of select="@id" />'); return false;</xsl:attribute>
								<xsl:attribute name="href">index.php?id=<xsl:value-of select="@id" /></xsl:attribute>
								<span class="icon">
									<img src=".themes/ajax/foldericon.png" alt="Folder Icon" />
								</span>
								<span class="name"><xsl:value-of select="@name" /></span>
							</a>
						</li>
					</xsl:for-each>
				</ul>
			</li>
		</ul>
	</xsl:template>

	<xsl:template match="image" mode="post">
		<xsl:variable name="path">
			<xsl:call-template name="replaceCharsInString">
				<xsl:with-param name="stringIn">
					<xsl:text>./</xsl:text><xsl:apply-templates select="ancestor::folder" mode="getpath" /><xsl:value-of select="@file" />
				</xsl:with-param>
				<xsl:with-param name="charsIn" select="' '"/>
				<xsl:with-param name="charsOut" select="'%20'"/>
			</xsl:call-template>
		</xsl:variable>

		<div id="breadcrumbtrail">
			<xsl:apply-templates select="parent::folder" mode="breadcrumbtrail" />
		</div>

		<div id="albums">
			<div id="expandedimage">
				<div id="linktospan">
					<a>
						<xsl:attribute name="href">index.php?id=<xsl:value-of select="@id" /></xsl:attribute>
						<xsl:text>Link to this image</xsl:text>
					</a>
				</div>
				<div id="expandedimagespan">
					<a href="#" onclick="closeImage(); return false;">
						<img>
							<xsl:attribute name="src"><xsl:value-of select="$path" /></xsl:attribute>
							<xsl:attribute name="alt"><xsl:value-of select="@file" /></xsl:attribute>
						</img>
					</a>
				</div>
				<div class="name"><xsl:value-of select="@file" /></div>
<div id="commentform">
	<div id="addcomment">
		<a href="#" onclick="toggleCommentsForm(); return false;">Add Comment</a>
	</div>
	<form action=".themes/db/postComment.php" method="post" style="display: none;" onsubmit="return addNewComment();">
		<fieldset>
			<input type="hidden" name="ajax" value="0" />
			<input type="hidden" name="image_id">
				<xsl:attribute name="value"><xsl:value-of select="@id" /></xsl:attribute>
			</input>
			<label>
				<span>Name: </span>
				<input type="text" name="user_name"/>
			</label>

			<label>
				<span>E-mail: </span>
				<input type="text" name="user_email"/>
			</label>

			<label>
				<span>Web: </span>
				<input type="text" name="user_web"/>
			</label>

			<label>
				<span>Comment: </span>
				<textarea cols="50" rows="3" name="user_comment"/>
			</label>

			<button type="submit">Submit</button>
		</fieldset>
	</form>
</div>
			</div>

			<xsl:if test="count(comment) > 0">
				<div id="comments">
					<xsl:apply-templates select="comment" />
				</div>
			</xsl:if>

			<div id="selectedalbum">
				<xsl:apply-templates select="parent::*/folder" />
				<xsl:apply-templates select="parent::*/image">
					<xsl:with-param name="selectedID" select="@id" />
				</xsl:apply-templates>
			</div>

		</div>
	</xsl:template>

	<xsl:template match="comment">
		<div class="comment">
			<xsl:choose>
				<xsl:when test="position() mod 2 = 0">
					<xsl:attribute name="class">comment odd</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="class">comment</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<span class="date"><xsl:value-of select="added" /></span>
			<span class="name">
				<xsl:choose>
					<xsl:when test="email != ''">
						<a>
							<xsl:attribute name="href">mailto:<xsl:value-of select="email" /></xsl:attribute>
							<xsl:value-of select="name" />
						</a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="user_name" />
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<span class="web">
				<xsl:choose>
					<xsl:when test="www != ''">
						<a>
							<xsl:attribute name="href">http://<xsl:value-of select="www" /></xsl:attribute>
							<xsl:value-of select="www" />
						</a>
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<span class="commenttext">
				<xsl:call-template name="nl2br">
					<xsl:with-param name="contents" select="comment_text" />
				</xsl:call-template>
			</span>
			<span class="rating">
				<xsl:value-of select="rating" />
			</span>
		</div>
	</xsl:template>

	<xsl:template match="folder" mode="getpath">
		<xsl:value-of select="concat(@name, '/')" />
	</xsl:template>

	<xsl:template name="replaceCharsInString">
		<xsl:param name="stringIn"/>
		<xsl:param name="charsIn"/>
		<xsl:param name="charsOut"/>
		<xsl:choose>
			<xsl:when test="contains($stringIn,$charsIn)">
				<xsl:value-of select="concat(substring-before($stringIn,$charsIn),$charsOut)"/>
				<xsl:call-template name="replaceCharsInString">
					<xsl:with-param name="stringIn" select="substring-after($stringIn,$charsIn)"/>
					<xsl:with-param name="charsIn" select="$charsIn"/>
					<xsl:with-param name="charsOut" select="$charsOut"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$stringIn"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="nl2br">
		<xsl:param name="contents" />
		<xsl:choose>
			<xsl:when test="contains($contents, '&#10;')">
					<xsl:value-of select="substring-before($contents, '&#10;')" />
					<br />
					<xsl:call-template name="nl2br">
						<xsl:with-param name="contents" select="substring-after($contents, '&#10;')" />
					</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
					<xsl:value-of select="$contents" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
