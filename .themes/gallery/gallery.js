function getElementsByClassName(className, tag){
	var testClass = new RegExp("(^|\\s)" + className + "(\\s|$)");
	tag = tag || "*";
	var elm = document;
	var elements = (tag == "*" && elm.all)? elm.all : elm.getElementsByTagName(tag);
	var returnElements = [];
	var current;
	var length = elements.length;
	for(var i=0; i<length; i++){
		current = elements[i];
		if(testClass.test(current.className)){
			returnElements.push(current);
		}
	}
	return returnElements;
}

function init()
{
	var ul_nodes = document.getElementsByTagName("ul");
	
	for(var i = 0; i < ul_nodes.length; i++)
	{
		var parent_node = ul_nodes[i].parentNode;
		
		if (parent_node.nodeName == "LI")
		{
			ul_nodes[i].className += " hidden";
			// alert(parent_node.childNodes[1].nodeName);
			parent_node.setAttribute("onclick", "switchHidden(this, event)");
		}
	}
	
	
	// var folders = getElementsByClassName('folder', 'li');
}

function switchHidden(element, e)
{
	var target = e ? e.target : window.event.srcElement;
	
	// alert(target);
	if (target != element) return false;
	
	var child_nodes = element.childNodes;
	
	for(var i = 0; i < child_nodes.length; i++)
	{
		if (child_nodes[i].nodeName == "UL")
		{
			if (child_nodes[i].className.match("hidden"))
			{
				child_nodes[i].className = child_nodes[i].className.replace("hidden", ""); 
			}
			else
			{
				child_nodes[i].className += " hidden";
			}
		}
	}
	return false;
}

function show_image(url, name, id)
{
	var image_li = document.getElementById(id);
	var old_anchor = image_li.getElementsByTagName('a')[0];
	
	var new_anchor = document.createElement("a");
	// set the onclick
	new_anchor.setAttribute("onclick", "hide_image('" + url + "', '" + name + "', '" + id + "'); return false;");
	// set the href
	new_anchor.setAttribute("href", url);
	
	var new_image = document.createElement("img");
	new_image.setAttribute("src", url);
	
	new_anchor.appendChild(document.createTextNode(name));
	new_anchor.appendChild(new_image);
	
	image_li.replaceChild(new_anchor, old_anchor);
	
	return false;
}

function hide_image(url, name, id)
{
	var image_li = document.getElementById(id);
	var old_anchor = image_li.getElementsByTagName('a')[0];
	
	var new_anchor = document.createElement("a");
	// set the onclick
	new_anchor.setAttribute("onclick", "show_image('" + url + "', '" + name + "', '" + id + "'); return false;");
	// set the href
	new_anchor.setAttribute("href", url);
	
	var new_image = document.createElement("img");
	new_image.setAttribute("src", url.replace("./", ".thumbs/"));
	
	new_anchor.appendChild(document.createTextNode(name));
	new_anchor.appendChild(new_image);
	
	image_li.replaceChild(new_anchor, old_anchor);
	
	return false;
}