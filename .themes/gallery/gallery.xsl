<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output
		method="xml"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" 
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	/>
	
	<xsl:param name="title" />

	<xsl:template match="layout">
		<html>
			<head>
				<title><xsl:value-of select="$title" /></title>
				<link href=".themes/gallery/gallery.css" rel="stylesheet" type="text/css" />
				<script type="text/javascript" src=".themes/gallery/gallery.js"></script>
			</head>
			<body onload="init();">
				<h1><xsl:value-of select="$title" /></h1>
				<ul id="gallery">
					<xsl:apply-templates select="folder">
						<xsl:with-param name="location" select="@name" />
					</xsl:apply-templates>
				</ul>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="folder">
		<xsl:param name="location" />
		
		<li class="folder">
			<xsl:attribute name="id"><xsl:value-of select="generate-id()" /></xsl:attribute>
			<xsl:if test="count(folder) = 0">
				<xsl:attribute name="class">noborder</xsl:attribute>
			</xsl:if>
			
			<xsl:value-of select="@name" /><xsl:if test="count(folder) = 0 and count(image) = 0"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>(empty)</xsl:if>
			
			<xsl:if test="count(folder) > 0">
				<ul>
					<xsl:apply-templates select="folder">
						<xsl:with-param name="location" select="concat($location, '/', @name)" />
					</xsl:apply-templates>
				</ul>
			</xsl:if>
			
			<xsl:if test="count(image) > 0">
				<ul>
					<xsl:apply-templates select="image">
						<xsl:with-param name="location" select="concat($location, '/', @name)" />
					</xsl:apply-templates>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="image">
		<xsl:param name="location" />
		
		<li class="image">
			<xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
			
			<a>
				<xsl:attribute name="href"><xsl:value-of select="concat('.', $location, '/', .)" /></xsl:attribute>
				<xsl:attribute name="onclick">show_image('<xsl:value-of select="concat('.', $location, '/', @file)" />', '<xsl:value-of select="@file" />', '<xsl:value-of select="@id" />'); return false;</xsl:attribute>
				
				<xsl:value-of select="substring(@file, 0, " />
				
				<img>
					<xsl:attribute name="src"><xsl:value-of select="concat('.thumbs', $location, '/', @file)" /></xsl:attribute>
				</img>
			</a>
		</li>
	</xsl:template>

</xsl:stylesheet>
