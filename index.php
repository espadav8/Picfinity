<?php
// site options
$site_name = "Picfinity";

// default theme
$deault_theme = "clean";
$theme = $deault_theme;

// create thumbnails for images that don't have one already
$create_thumbnails = true;

// create a flat HTML version of the page
$create_html = false;

// rescan the folders (and thus recreate the XML for each page request)
$rescan_folders = false;

// create the gallery.xml file (for use by themes)
$create_xml = true;

// Allow the users to change the theme
// this needs the PHP version of the page
$enable_themes = true;

// thumbnail sizes
$thumbnail_width = 100;
$thumbnail_height = 100;

// use the database
$use_db = true;

// valid images to be shown
$valid_image_formats = array('png', 'gif', 'jpg', 'jpeg', 'bmp');

// here we either get the theme posted in
// or if there's one stored in the cookie use that one
// otherwise use the default one
if ($_POST['theme']) {
    setcookie("theme", $_POST['theme']);
    $theme = $_POST['theme'];
} elseif ($_COOKIE['theme']) {
    $theme = $_COOKIE['theme'];
}

$post_id = 0;

if ($_GET['id']) {
    $post_id = $_GET['id'];
}

if (($rescan_folders) || !file_exists("gallery.xml")) {
    if ($use_db) {
        require_once('db.inc');
        if (!$db_connection) {
            $use_db = false;
        } else {
            if ($selected_table) {
                $result = mysql_query("SELECT image_md5 FROM images");
                if (!$result) {
                    echo 'Invalid query: ' . mysql_error() . "\n";
                    echo 'Whole query: ' . $query;
                    $use_db = false;
                } else {
                    $current_images = array();
                    while ($row = mysql_fetch_row($result)) {
                        $current_images[] = $row[0];
                    }
                    mysql_free_result($result);
                }
            } else {
                $use_db = false;
            }
        }
    }

    // get the layout of the current dir
    $site_layout_xml = create_folder_layout('.');

    // create the XML file if we've been told to
    if ($create_xml) {
        $fh = fopen("gallery.xml", "w");
        fwrite($fh, $site_layout_xml);
        fclose($fh);
    }

    if ($use_db) {
        mysql_close($db_connection);
    }
} else {
    $fh = fopen("gallery.xml", "r");
    $site_layout_xml = fread($fh, filesize("gallery.xml"));
    fclose($fh);
}

// pass the XML to the theme and get the HTML back
$html = xml_to_xhtml($site_layout_xml, ".themes/$theme/$theme.xsl",
    array('title' => $site_name, 'post_id' => $post_id));

// create the flat version if we need to
if ($create_html) {
    $fh = fopen("index.html", "w");
    fwrite($fh, $html);
    fclose($fh);
}

// echo the HTML to the screen
echo $html;


// this function takes a folder path and creates a layout of the folder and sub folders
function create_folder_layout($folder)
{
    global $valid_image_formats, $create_thumbnails, $enable_themes, $use_db, $current_images, $post_id;

    // get the name of the folder or '' if it's the root
    $name = substr($folder, strrpos($folder, '/') + 1);

    // create a folder ID
    $folder_id = "id" . md5($folder);

    // create the XML
    if ($name == "") {
        $folder_layout_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            . "<!DOCTYPE layout PUBLIC \"-//picfinity//Gallery Layout//EN\" \"http://www.espadav8.co.uk/gallery/gallery.dtd\">\n"
            . "<layout id=\"$folder_id\" name=\"\">\n";
    } else {
        $folder_layout_xml = "<folder id=\"$folder_id\" name=\"$name\">\n";
    }

    // get a listing of the files/folder
    $directoryHandle = opendir($folder);
    while (false !== ($filename = readdir($directoryHandle))) {
        $folder_contents[] = $filename;
    }

    sort($folder_contents);

    // for each entry
    while (list(, $folder_entry) = each($folder_contents)) {
        // if it's a folder and the path of it is ./.themes then create the list of themes
        if ((($folder . '/' . $folder_entry) == "./.themes") &&
            (is_dir('./themes')) &&
            $enable_themes
        ) {
            $folder_layout_xml .= create_themes_layout();
        } // else if it starts with a . ignore it
        else {
            if (strpos($folder_entry, '.') === 0) {
                continue;
            } // if it's a directory and doesn't start with a . then create a folder layout for it
            else {
                if (is_dir($folder . '/' . $folder_entry)) {
                    // get a list of it's files and check/create thumbnail(s)
                    $folder_layout_xml .= create_folder_layout($folder . '/' . $folder_entry);
                } // otherwise check to see if it's an image
                else {
                    $ext = strtolower(substr($folder_entry,
                        strrpos($folder_entry, '.') + 1));

                    if (in_array($ext, $valid_image_formats)) {
                        // create a thumbnail if we've been told to
                        if (($create_thumbnails) && (!file_exists('.thumbs/' . $folder . '/' . $folder_entry))) {
                            create_thumbnail($folder, $folder_entry, $ext);
                        }

                        $image_id = "id" . md5($folder . '/' . $folder_entry);

                        if (!in_array($image_id, $current_images)) {
                            add_image_to_db($image_id);
                        }

                        if (($use_db) && ($image_id === $post_id)) {
                            $folder_layout_xml .= "\t<image id=\"$image_id\" file=\"$folder_entry\">\n"
                                . get_comments($image_id)
                                . "\t</image>\n";
                        } else {
                            $folder_layout_xml .= "\t<image id=\"$image_id\" file=\"$folder_entry\" />\n";
                        }
                    }
                }
            }
        }
    }

    // close up the XML
    if ($name == "") {
        $folder_layout_xml .= "</layout>\n";
    } else {
        $folder_layout_xml .= "</folder>\n";
    }

    // return the XML
    return $folder_layout_xml;
}

function add_image_to_db($image_id)
{
    $query = "INSERT INTO images (image_md5) VALUES ('$image_id')";
    $result = mysql_query($query);
    if (!$result) {
        // die (mysql_error());
    }
}

function get_comments($image_id)
{
    $query = "
                SELECT        user_name
                ,            user_www
                ,            user_email
                ,            user_comment
                ,            comment_rating
                ,            date_added
                FROM            comments
                INNER JOIN    images
                WHERE        images.image_id = comments.image_id
                AND            images.image_md5 = '$image_id'
                ORDER BY        date_added DESC";

    $result = mysql_query($query);

    if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_assoc($result)) {
            $comments_xml .= "<comment>\n"
                . "<name>$row[user_name]</name>\n"
                . "<www>$row[user_www]</www>\n"
                . "<email>$row[user_email]</email>\n"
                . "<comment_text>" . htmlspecialchars($row['user_comment']) . "</comment_text>\n"
                . "<rating>$row[comment_rating]</rating>\n"
                . "<added>$row[date_added]</added>\n"
                . "</comment>\n";
        }

        return $comments_xml;
    } else {
        return "";
    }
}

function create_themes_layout()
{
    $themes_id = "id" . md5(".themes");

    // create the themes node
    $theme_xml = "<themes id=\"$themes_id\" name=\".themes\">\n";

    // get the themes
    $directoryHandle = opendir(".themes");
    while (false !== ($filename = readdir($directoryHandle))) {
        $folder_contents[] = $filename;
    }

    sort($folder_contents);

    // for each entry
    while (list(, $folder_entry) = each($folder_contents)) {
        // if it's hidden ignore it
        if (strpos($folder_entry, '.') === 0) {
            continue;
        } // otherwise if it's a dir assume it's a theme and add it to the list
        else {
            if (is_dir(".themes/" . $folder_entry)) {
                $theme_id = md5(".themes/" . $folder_entry);
                $theme_xml .= "\t<theme id=\"$theme_id\" name=\"$folder_entry\" />\n";
            }
        }
    }

    $theme_xml .= "</themes>\n";

    return $theme_xml;
}

function create_thumbnail($folder, $image, $ext)
{
    global $thumbnail_width;
    global $thumbnail_height;

    if (!is_dir('.thumbs/' . $folder)) {
        MakeDirectory('.thumbs/' . $folder, 0777);
        chmod('.thumbs/' . $folder, 0777);
    }

    $src_img;
    switch ($ext) {
        case 'png':
            $src_img = imagecreatefrompng($folder . '/' . $image);
            break;
        case 'jpg':
        case 'jpeg':
            $src_img = imagecreatefromjpeg($folder . '/' . $image);
            break;
        case 'gif':
            $src_img = imagecreatefromgif($folder . '/' . $image);
            break;
        case 'bmp':
            $src_img = imagecreatefrombmp($folder . '/' . $image);
            break;
        default:
            return;
            break;
    }

    $old_x = imageSX($src_img);
    $old_y = imageSY($src_img);

    if ($old_x > $old_y) {
        $thumb_w = $thumbnail_width;
        $thumb_h = $old_y * ($thumbnail_height / $old_x);
    } else {
        if ($old_x < $old_y) {
            $thumb_w = $old_x * ($thumbnail_width / $old_y);
            $thumb_h = $thumbnail_height;
        } else {
            if ($old_x == $old_y) {
                $thumb_w = $thumbnail_width;
                $thumb_h = $thumbnail_height;
            }
        }
    }

    $dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);
    imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_w, $thumb_h,
        $old_x, $old_y);

    switch ($ext) {
        case 'png':
            imagepng($dst_img, '.thumbs/' . $folder . '/' . $image);
            break;
        case 'jpg':
        case 'jpeg':
            imagejpeg($dst_img, '.thumbs/' . $folder . '/' . $image);
            break;
        case 'gif':
            imagegif($dst_img, '.thumbs/' . $folder . '/' . $image);
            break;
        case 'bmp':
            imagebmp($dst_img, '.thumbs/' . $folder . '/' . $image);
            break;
    }

    imagedestroy($dst_img);
    imagedestroy($src_img);

    return;
}

function xml_to_xhtml($xml, $xsl_file, $params = array())
{
    $ver = explode('.', PHP_VERSION);
    $ver_num = $ver[0] . $ver[1] . $ver[2];

    if ($ver_num < 500) {
        $arguments = array('/_xml' => $xml);
        $xsltproc = xslt_create();
        $html = xslt_process($xsltproc, 'arg:/_xml', $xsl_file, null,
            $arguments);

        if (empty($html)) {
            die('XSLT processing error: ' . xslt_error($xsltproc));
        }
        xslt_free($xsltproc);
        return $html;
    } else {
        $doc = new DOMDocument();
        $xsl = new XSLTProcessor();

        $doc->load($xsl_file);
        $xsl->importStyleSheet($doc);

        $doc->loadXML($xml);
        $xsl->setParameter('', $params);

        return $xsl->transformToXML($doc);
    }
}

function MakeDirectory($dir, $mode = 0755)
{
    if (is_dir($dir) || @mkdir($dir, $mode)) {
        return true;
    }
    if (!MakeDirectory(dirname($dir), $mode)) {
        return false;
    }
    return @mkdir($dir, $mode);
}
