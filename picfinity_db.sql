CREATE DATABASE IF NOT EXISTS picfinity;

CREATE TABLE IF NOT EXISTS picfinity.images (
	image_id INT UNSIGNED NOT NULL AUTO_INCREMENT KEY,
	image_md5 CHAR(34),
	title CHAR(255),
	description TEXT,
	views INT UNSIGNED DEFAULT 0,
	rating INT DEFAULT 0
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS picfinity.comments (
	comment_id INT UNSIGNED NOT NULL AUTO_INCREMENT KEY,
	image_id INT UNSIGNED NOT NULL,
	user_comment TEXT,
	user_ip INT(15) NOT NULL DEFAULT 0,
	user_name CHAR(255),
	user_email CHAR(255),
	user_www CHAR(255),
	comment_rating INT DEFAULT 0,
	approved TINYINT(1) DEFAULT 0,
	date_added TIMESTAMP,
	FOREIGN KEY (image_id) REFERENCES images(image_id) ON DELETE CASCADE
	) ENGINE = InnoDB;
